flatpak-xdg-utils (1.0.6-1) unstable; urgency=medium

  * New upstream release
    - Drop patches, applied upstream
  * d/gbp.conf: Use debian/latest as packaging branch
  * d/gbp.conf: Use default compression
  * d/copyright: Update
  * Standards-Version: 4.7.0 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Tue, 03 Sep 2024 15:02:18 +0100

flatpak-xdg-utils (1.0.5-3) unstable; urgency=medium

  * Depend on dbus-daemon for tests, not dbus.
    The tests start their own mock session dbus-daemon, and do not need
    the standard session or system bus.

 -- Simon McVittie <smcv@debian.org>  Thu, 08 Dec 2022 10:39:09 +0000

flatpak-xdg-utils (1.0.5-2) unstable; urgency=medium

  * d/watch: Adapt to Github website changes
  * d/p/flatpak-spawn-Fix-memory-leak-when-receiving-NameOwnerCha.patch:
    Add patch from upstream to fix a memory leak
  * d/p/Show-permission-hint-for-flatpk-spawn-host-60.patch:
    Add patch from upstream to show a hint about permissions when
    appropriate
  * Standards-Version: 4.6.1 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Sat, 12 Nov 2022 17:53:41 +0000

flatpak-xdg-utils (1.0.5-1) unstable; urgency=medium

  * New upstream release
    - Fix xdg-email attachment passing
    - Fix leaks
    - New flatpak-spawn features: --app-path, --usr-path, --unset-env,
      --env-fd, --share-pid
    - flatpak-spawn --sandbox-expose-path now tries to normalize paths
      to work better with portals
  * Standards-Version: 4.6.0 (no changes required)
  * Use debhelper compat level 13, drop dh_missing override

 -- Simon McVittie <smcv@debian.org>  Fri, 11 Feb 2022 10:18:54 +0000

flatpak-xdg-utils (1.0.4-1) unstable; urgency=medium

  * New upstream release
    - In flatpak-spawn, close fds after they have been forwarded

 -- Simon McVittie <smcv@debian.org>  Fri, 09 Oct 2020 08:52:05 +0100

flatpak-xdg-utils (1.0.3-1) unstable; urgency=medium

  * New upstream release
    - Better compatibility with xdg-desktop-portal 1.8.0

 -- Simon McVittie <smcv@debian.org>  Mon, 05 Oct 2020 13:11:14 +0100

flatpak-xdg-utils (1.0.2-1) unstable; urgency=medium

  * New upstream release
    - Drop patch, applied upstream
  * Set upstream metadata fields: Repository.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * Standards-Version: 4.5.0 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Sun, 16 Aug 2020 17:13:55 +0100

flatpak-xdg-utils (1.0.1-2) unstable; urgency=medium

  * d/p/xdg-email-xdg-open-Terminate-GOptionEntry-array-correctly.patch:
    Add proposed patch to fix crashes on some architectures

 -- Simon McVittie <smcv@debian.org>  Mon, 25 Nov 2019 18:53:04 +0000

flatpak-xdg-utils (1.0.1-1) unstable; urgency=medium

  * New upstream release
    - Drop all patches, applied upstream
  * d/salsa-ci.yml: Request standard CI on salsa.debian.org
  * Remove redundant --libexecdir=/usr/libexec since this is the default
    at debhelper compat level 12
  * Standards-Version: 4.4.1 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Mon, 25 Nov 2019 08:34:44 +0000

flatpak-xdg-utils (1.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #919299)

 -- Simon McVittie <smcv@debian.org>  Fri, 18 Jan 2019 10:29:06 +0000
